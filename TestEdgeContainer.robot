*** Settings ***
Library     SeleniumLibrary

Variables   ./locators.py

*** Test Cases ***
Testing Open Browser Container
    SLEEP    5s
    OPEN BROWSER    https://www.google.com    ${BROWSER}    remote_url=http://${HOST}:4444/wd/hub
    SLEEP    5s
    CAPTURE PAGE SCREENSHOT    EMBED
    CLOSE ALL BROWSERS