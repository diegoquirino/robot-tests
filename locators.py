import socket
from datetime import datetime


# Project Variables
BROWSER = "edge"
HOST = socket.gethostbyname(socket.gethostname())
VERSION = "4.0.0-beta-1-20210215"
GRID = f'{datetime.now().strftime("%Y%m%d%H%M%S")}_grid'
