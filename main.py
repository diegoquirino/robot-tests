import os

from locators import BROWSER, HOST, VERSION, GRID


def execute(cmd: str):
    res = os.system(cmd)
    print(f'EXECUTED: "{cmd}" WITH RESULT {res}')


if __name__ == '__main__':
    execute(f'docker network create {GRID}')
    execute(f'docker run -d -p 4444:4444 -p 6900:5900 --net {GRID} --name selenium '
            f'-v /dev/shm:/dev/shm '
            f'selenium/standalone-{BROWSER}:{VERSION}')
    print(f'Server initialized at host {HOST}:4444')
    execute(f'robot --outputdir output *.robot')
    execute(f'docker stop selenium && docker rm selenium')
    execute(f'docker network rm {GRID}')


